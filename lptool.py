import os
import json
import glob
import pdb
import collections
import nltk
from pprint import pprint
import runeapi

letters=[]
indices=[str(x) for x in range(29)]
indices.append("-")

with open('./rune.json') as data:
    runes = json.load(data)
    for r in runes:
        letters.append(runes[r]["letter"])

text = ''
files = glob.glob(os.path.join('./liber_primus/json/', '*.json'))

def TranslateIndicesToEnglish(fragment):
    return ''.join(runes[r]["letter"] if r in indices else r for r in fragment.replace("\n", "").split(" "))

def TranslateEnglishToGematria(fragment):
    return runeapi.translate_to_gematria(fragment)


deleteDoubleSentence = False
files = sorted(files)
for pgfile in files:
    with open(pgfile) as page:
        pg = json.load(page)
    sectionnumber = pg["section"]
    pagenumber = pg["page_number"]
    nextpage = pg["continues"]
    ciphertext = pg["ciphertext"]
    if deleteDoubleSentence:
       del ciphertext[0]
       if sectionnumber == "8":
            text+="\n\n"
            print("Section 8 stop")
#            text +="\n---  Section " + sectionnumber + " Stop ---"
       deleteDoubleSentence = False
    for sentence in ciphertext:
        for fragment in sentence:
            text += TranslateIndicesToEnglish(fragment)
        if sentence == ciphertext[-1]:
            if nextpage == "none":
                text += "\n\n"
#                text +="\n---  Section " + sectionnumber + " Stop ---"
                print("---  Section " + sectionnumber +" Stop ---")
            elif nextpage == "next":
                print("--- Sentence ends at end of page ---")
            else:
                print("--- Sentence continues on to next page, retrieve it ---")
                deleteDoubleSentence = True
                with open(files[int(nextpage)]) as nextpg:
                    sentenceContinued = json.load(nextpg)["ciphertext"][0]
                    text += TranslateIndicesToEnglish(''.join(sentenceContinued))
        text += ' '
    text += ' '

print(text)

tokens = nltk.word_tokenize(text)

bigrams = nltk.bigrams(tokens)

freqdist = nltk.FreqDist(bigrams)

print(collections.Counter(text.split(" ")))

pdb.set_trace()

#for k,v in freqdist.items():
#    print(k,v)
