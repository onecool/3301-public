import json
import pdb
import re


#Change between bacon_a, bacon_b, bacon_c
with open('./bacon_c.json') as data:
    baconAlphabet = json.load(data)

# Either 0 = a, 1 = b, or 1 = a, 0 = b
mapping = {"0":"b", "1":"a"};

# Read in spacing data from Liber Primus
with open('./spacing.txt') as data:
    spacings = data.readlines()
    spacings = spacings[0]
    spacings = spacings.replace('\n','')

#Replace 0/1 with a/b
for k in mapping.keys():
    spacings = spacings.replace(k, mapping[k])

# Break into 5-grams
grams = re.findall('.....', spacings)

# Look up 5-grams according to bacon alphabet
output = [baconAlphabet[r] if r in baconAlphabet.keys() else r for r in grams]

print(output)



